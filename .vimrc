set fenc=utf-8

" --- Base ---
set nobackup
set noswapfile
set hidden
set autoread

" --- Display ---
set ruler
set number
set title
set list
set ambiwidth=double
set cmdheight=2
set laststatus=2
set list listchars=tab:\▸\-,trail:-,eol:↲,extends:»,precedes:«,nbsp:%
set nrformats-=octal
set display=lastline
set pumheight=10

" --- Insert Mode ---
set showcmd
set backspace=indent,eol,start
set virtualedit=onemore
set smartindent
set visualbell
set showmatch
set matchtime=1
set wildmode=list:longest

" --- Tabs ---
set expandtab
set tabstop=2
set shiftwidth=2

" --- Search ---
set ignorecase
set smartcase
set incsearch
set wrapscan
set hlsearch

" --- Key Mappings ---
nnoremap <Space>w :w<CR>
nnoremap <Space>q :q<CR>
nnoremap <Space>W :wq!<CR>
nnoremap <Space>Q :q!<CR>
nnoremap Y y$
nnoremap + <C-a>
nnoremap - <C-x>
inoremap jk <Esc>
